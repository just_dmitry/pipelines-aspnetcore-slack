Sample script for building ASP.NET Core app with Bitbucket Pipelines and pushing build status into Slack.

## 1. Prepare Slack webhook url

In your **Slack** team create incoming webhook integration and obtain **Webhook URL** like this: `https://hooks.slack.com/services/xxxx/xxxx/xxxxxxxx` (also, don't forget to customize name and icon there).

Go to your account Bitbucket settings, and under `Pipelines`/`Environment variables` section add new variable with name `SLACK_BUILDSTATUS_URL` and webhook url (from above) in value.

## 2. Download and modify `build.sh`

Download [build.sh](https://bitbucket.org/just_dmitry/pipelines-aspnetcore-slack/raw/master/build.sh) file from this repository to root of your repository.

Open it in text editor and put correct paths to your project directories in lines 3-4:

```
dotnet build src/website        # Put your build project dir here
dotnet test test/website.tests  # Put your test project dir here
```

## 3. Create `bitbucket-pipelines.yml`

Create `bitbucket-pipelines.yml` file in root of you repo with content:

```
image: microsoft/dotnet

pipelines:
  default:
    - step:
        script:
          - dotnet restore
          - chmod 755 build.sh
          - ./build.sh
```

Now after each build you will receive message like this:

![sample.png](sample.png)